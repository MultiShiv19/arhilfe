package com.arhilfe;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Shiva on 5/8/2017.
 */

public interface Interface {

    //This method is used for "GET"
    @GET("generate.php")
    Call<ServerResponse> get(
            @Query("method") String method,
            @Query("username") String username
    );

}