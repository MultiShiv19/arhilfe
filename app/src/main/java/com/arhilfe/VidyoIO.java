package com.arhilfe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.vidyo.VidyoClient.Connector.Connector;
import com.vidyo.VidyoClient.Connector.VidyoConnector;
import com.vidyo.VidyoClient.Device.VidyoDevice;
import com.vidyo.VidyoClient.Device.VidyoLocalMicrophone;
import com.vidyo.VidyoClient.Device.VidyoLocalSpeaker;
import com.vidyo.VidyoClient.Device.VidyoRemoteMicrophone;
import com.vidyo.VidyoClient.Endpoint.VidyoChatMessage;
import com.vidyo.VidyoClient.Endpoint.VidyoParticipant;


public class VidyoIO extends AppCompatActivity implements
        VidyoConnector.IConnect,
        VidyoConnector.IRegisterLocalMicrophoneEventListener,
        VidyoConnector.IRegisterLocalSpeakerEventListener,
        VidyoConnector.IRegisterRemoteMicrophoneEventListener,
        VidyoConnector.IRegisterMessageEventListener{

    private VidyoConnector vc;
    private FrameLayout videoFrame;
    private EditText userName;
    private EditText roomName;
    private Communicator communicator;
    private String token;
    private String username;
    private String roomname;
    private Button switchCam;

    public VidyoIO() {
        vc = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vidyo_io);

        Connector.SetApplicationUIContext(this);
        Connector.Initialize();
        videoFrame = (FrameLayout) findViewById(R.id.vidyoFrame);
        userName = (EditText) findViewById(R.id.userName);
        roomName = (EditText) findViewById(R.id.roomName);
        switchCam = (Button) findViewById(R.id.switchCamera);

        switchCam.setVisibility(View.INVISIBLE);

        communicator = new Communicator();
    }

    public void Connect(View v) {
        if(vc == null)
            vc = new VidyoConnector(videoFrame, VidyoConnector.VidyoConnectorViewStyle.VIDYO_CONNECTORVIEWSTYLE_Default, 16, "", "", 0);
        vc.ShowViewAt(videoFrame, 0, 0, videoFrame.getWidth(), videoFrame.getHeight());

        switchCam.setVisibility(View.VISIBLE);
        username = userName.getText().toString();
        roomname = roomName.getText().toString();
        useGet(username);
    }

    public void Disconnect(View v) {
        vc.Disconnect();
        videoFrame.removeAllViews();
        vc = null;
        switchCam.setVisibility(View.INVISIBLE);
    }

    private void useGet(String username){
        communicator.tokenGet(username);
    }

    public void OnSuccess() {

    }

    public void OnFailure(VidyoConnector.VidyoConnectorFailReason vidyoConnectorFailReason) {

    }

    public void OnDisconnected(VidyoConnector.VidyoConnectorDisconnectReason vidyoConnectorDisconnectReason) {

    }

    @Override
    public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onServerEvent(ServerEvent serverEvent){
        token = serverEvent.getServerResponse().getToken();
        vc.Connect("prod.vidyo.io", token, username, roomname, this);

    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent){
        Toast.makeText(this,""+errorEvent.getErrorMsg(),Toast.LENGTH_LONG).show();
    }


    @Override
    public void OnLocalMicrophoneAdded(VidyoLocalMicrophone vidyoLocalMicrophone) {
        vc.OnLocalMicrophoneAdded(vidyoLocalMicrophone);
    }

    @Override
    public void OnLocalMicrophoneRemoved(VidyoLocalMicrophone vidyoLocalMicrophone) {
        vc.OnLocalMicrophoneRemoved(vidyoLocalMicrophone);
    }

    @Override
    public void OnLocalMicrophoneSelected(VidyoLocalMicrophone vidyoLocalMicrophone) {
        vc.OnLocalMicrophoneSelected(vidyoLocalMicrophone);
    }

    @Override
    public void OnLocalMicrophoneStateUpdated(VidyoLocalMicrophone vidyoLocalMicrophone, VidyoDevice.VidyoDeviceState vidyoDeviceState) {
        vc.OnLocalMicrophoneStateUpdated(vidyoLocalMicrophone, vidyoDeviceState);
    }

    @Override
    public void OnLocalSpeakerAdded(VidyoLocalSpeaker vidyoLocalSpeaker) {
        vc.OnLocalSpeakerAdded(vidyoLocalSpeaker);
    }

    @Override
    public void OnLocalSpeakerRemoved(VidyoLocalSpeaker vidyoLocalSpeaker) {
        vc.OnLocalSpeakerRemoved(vidyoLocalSpeaker);
    }

    @Override
    public void OnLocalSpeakerSelected(VidyoLocalSpeaker vidyoLocalSpeaker) {
        vc.OnLocalSpeakerSelected(vidyoLocalSpeaker);
    }

    @Override
    public void OnLocalSpeakerStateUpdated(VidyoLocalSpeaker vidyoLocalSpeaker, VidyoDevice.VidyoDeviceState vidyoDeviceState) {
        vc.OnLocalSpeakerStateUpdated(vidyoLocalSpeaker, vidyoDeviceState);
    }


    @Override
    public void OnRemoteMicrophoneAdded(VidyoRemoteMicrophone vidyoRemoteMicrophone, VidyoParticipant vidyoParticipant) {
        vc.OnRemoteMicrophoneAdded(vidyoRemoteMicrophone, vidyoParticipant);
    }

    @Override
    public void OnRemoteMicrophoneRemoved(VidyoRemoteMicrophone vidyoRemoteMicrophone, VidyoParticipant vidyoParticipant) {
        vc.OnRemoteMicrophoneRemoved(vidyoRemoteMicrophone, vidyoParticipant);
    }

    @Override
    public void OnRemoteMicrophoneStateUpdated(VidyoRemoteMicrophone vidyoRemoteMicrophone, VidyoParticipant vidyoParticipant, VidyoDevice.VidyoDeviceState vidyoDeviceState) {
        vc.OnRemoteMicrophoneStateUpdated(vidyoRemoteMicrophone, vidyoParticipant, vidyoDeviceState);
    }

    @Override
    public void OnChatMessageReceived(VidyoParticipant vidyoParticipant, VidyoChatMessage vidyoChatMessage) {

    }

    public void SwitchCamera(View v) {
        vc.CycleCamera();
    }
}
