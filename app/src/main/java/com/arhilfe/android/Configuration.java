package com.arhilfe.android;

import android.support.v4.app.Fragment;

import com.arhilfe.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Configuration {

    private static final List<Feature> features = new ArrayList<Feature>();

    static {
//        addFeature("user_identity", R.mipmap.user_identity, R.string.feature_sign_in_title,
//                R.string.feature_sign_in_subtitle, R.string.feature_sign_in_overview,
//                R.string.feature_sign_in_description, R.string.feature_sign_in_powered_by,
//                new Item(R.string.main_fragment_title_user_identity, R.mipmap.user_identity,
//                        R.string.feature_sign_in_button, IdentityFragment.class));
        addFeature("push_notification", R.mipmap.push,
                R.string.feature_push_notifications_title,
                R.string.feature_push_notifications_subtitle,
                R.string.feature_push_notifications_overview,
                R.string.feature_push_notifications_description,
                R.string.feature_push_notifications_powered_by,
                new Item(R.string.main_fragment_title_push_notification, R.mipmap.push,
                        R.string.feature_push_notifications_button, PushFragment.class));
        addFeature("content_delivery", R.mipmap.content_delivery,
                R.string.feature_app_content_delivery_title,
                R.string.feature_app_content_delivery_subtitle,
                R.string.feature_app_content_delivery_overview,
                R.string.feature_app_content_delivery_description,
                R.string.feature_app_content_delivery_powered_by,
                new Item(R.string.main_fragment_title_content_delivery,
                        R.mipmap.content_delivery,
                        R.string.feature_app_content_delivery_button,
                        ContentDeliveryFragment.class));

    }

    public static List<Feature> getFeatureList() {
        return Collections.unmodifiableList(features);
    }

    public static Feature getFeatureByName(final String name) {
        for (Feature feature : features) {
            if (feature.name.equals(name)) {
                return feature;
            }
        }
        return null;
    }

    private static void addFeature(final String name, final int iconResId, final int titleResId,
                                   final int subtitleResId, final int overviewResId,
                                   final int descriptionResId, final int poweredByResId,
                                   final Item... items) {
        Feature feature = new Feature(name, iconResId, titleResId, subtitleResId,
                overviewResId, descriptionResId, poweredByResId, items);
        features.add(feature);
    }

    public static class Feature {
        public String name;
        public int iconResId;
        public int titleResId;
        public int subtitleResId;
        public int overviewResId;
        public int descriptionResId;
        public int poweredByResId;
        public List<Item> rows;

        public Feature() {

        }

        public Feature(final String name, final int iconResId, final int titleResId,
                       final int subtitleResId, final int overviewResId,
                       final int descriptionResId, final int poweredByResId,
                       final Item... items) {
            this.name = name;
            this.iconResId = iconResId;
            this.titleResId = titleResId;
            this.subtitleResId = subtitleResId;
            this.overviewResId = overviewResId;
            this.descriptionResId = descriptionResId;
            this.poweredByResId = poweredByResId;
            this.rows = Arrays.asList(items);
        }
    }

    public static class Item {
        public int titleResId;
        public int iconResId;
        public int buttonTextResId;
        public String fragmentClassName;

        public String title;
        public String buttonText;
        public Serializable tag ;

        public Item(final int titleResId, final int iconResId, final int buttonTextResId,
                    final Class<? extends Fragment> fragmentClass) {
            this.titleResId = titleResId;
            this.iconResId = iconResId;
            this.buttonTextResId = buttonTextResId;
            this.fragmentClassName = fragmentClass.getName();
        }

        public Item(final String title, final String buttonText, final Serializable tag, final Class<? extends  Fragment> fragmentClass){
            this.title = title;
            this.buttonText = buttonText;
            this.tag = tag;
            this.fragmentClassName = fragmentClass.getName();
        }
    }
}
