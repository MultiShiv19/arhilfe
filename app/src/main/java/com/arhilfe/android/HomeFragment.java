package com.arhilfe.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arhilfe.MainActivity;
import com.arhilfe.R;
import com.vuforia.samples.VuforiaSamples.app.ObjectRecognition.ObjectTargets;

public class HomeFragment extends FragmentBase {

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView allItemList = (ListView) getActivity().findViewById(R.id.allItemList);

        allItemList.setAdapter(MainActivity.allTrackablesAdapter);

        allItemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MainActivity.selected = MainActivity.allTrackables.get(position);
                Intent i = new Intent(getActivity(), ObjectTargets.class);
                startActivity(i);

            }
        });
    }
}
