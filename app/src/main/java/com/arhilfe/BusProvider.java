package com.arhilfe;

import com.squareup.otto.Bus;

/**
 * Created by Shiva on 5/8/2017.
 */

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance(){
        return BUS;
    }

    public BusProvider(){}
}
