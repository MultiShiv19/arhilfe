package com.arhilfe;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Shiva on 5/8/2017.
 */

public class ServerResponse implements Serializable {
    @SerializedName("token")
    private String token;

    public ServerResponse(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
